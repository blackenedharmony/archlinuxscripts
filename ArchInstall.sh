# Arch Installer

TARGET_BOOT_SIZE=256M
TARGET_SWAP_SIZE=8G

if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root."
  exit
fi
lsblk
echo "Enter the name of the drive you want to install ArchLinux on"
echo "e.g /dev/sda"
read TARGET_DISK

read -p "All data will be wiped on the selected drive. Do you want to proceed? (y/n) " yn

case $yn in 
	y ) echo ok, proceeding;;
	n ) echo lol nerd...;
		exit;;
	* ) echo wrong answer;
		exit 1;;
esac

echo "alright formatting the disks"

sfdisk ${TARGET_DISK}  << END
size=$TARGET_BOOT_SIZE,bootable
size=$TARGET_SWAP_SIZE
;
END

mkfs.fat -F32 ${TARGET_DISK}2
mkfs.ext4 ${TARGET_DISK}2
mount ${TARGET_DISK}2 /mnt
mkdir /mnt/boot
mount ${TARGET_DISK}1 /mnt/boot
pacstrap /mnt base base-devel linux linux-firmware git nano grub efibootmgr networkmanager neofetch
genfstab -U /mnt >> /mnt/etc/fstab

echo "Install finished. Switching to the installed system."
echo "You're either on your own or going to use the chroot script."
echo "Good luck."
sleep 4
arch-chroot /mnt /bin/bash
